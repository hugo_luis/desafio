#language: pt

@valor
Funcionalidade: Valor
    validar operaçoes da Api  

    @get_valor
    Cenário: Validar GET API Marcas
        Quando faço uma requisição GET para o serviço fipe
        Então o serviço fipe deve responder com 200
        E retorna a lista de marcas

    @get_modelos
    Cenário: Validar GET API Marcas com id
        Quando faço uma requisição GET para o serviço fipe passando id
        Então o serviço fipe deve responder com 200
        E retorna o modelo

    @get_ano
    Cenário: Validar GET API Marcas com id e modelo
        Quando faço uma requisição GET para o serviço fipe passando modelo
        Então o serviço fipe deve responder com 200
        E retorna o ano
    
    @get_preco
    Cenário: Validar GET API Marcas com id e modelo e ano
        Quando faço uma requisição GET para o serviço fipe passando modelo e ano
        Então o serviço fipe deve responder com 200
        E retorna o preco
