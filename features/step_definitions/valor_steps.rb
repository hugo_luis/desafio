
Quando('faço uma requisição GET para o serviço fipe') do
    @request_valor= valor.get_valor
  end
  
  Então('o serviço fipe deve responder com {int}') do |status_code|
    expect(@request_valor.code).to eq status_code  
  end
  
  Então('retorna a lista de marcas') do
   expect(@request_valor.message).not_to be_empty
  end

  Quando('faço uma requisição GET para o serviço fipe passando id') do
    @id='23/modelos'
    @request_valor= valor.get_modelos(@id)
  end
  
  Então('retorna o modelo') do
    print @request_valor
    expect(@request_valor.message).not_to be_empty
  end

  Quando('faço uma requisição GET para o serviço fipe passando modelo') do
    @id='5637/anos'
    @request_valor= valor.get_ano(@id)
  end
  
  Então('retorna o ano') do
    print @request_valor
    expect(@request_valor.message).not_to be_empty
  end

  Quando('faço uma requisição GET para o serviço fipe passando modelo e ano') do
    @id='2016-1'
    @request_valor= valor.get_preco(@id)
  end
  
  Então('retorna o preco') do
    
    expect(@request_valor.message).not_to be_empty
    
    print @request_valor
  end
  

