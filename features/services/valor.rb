module Rest
    class Valor
        include HTTParty    

        headers 'Content-Type' => 'application/json'
        base_uri CONFIG['base_uri']

        def get_valor
            self.class.get("https://parallelum.com.br/fipe/api/v1/carros/marcas")

        end

        def get_modelos(id)
            self.class.get("https://parallelum.com.br/fipe/api/v1/carros/marcas/#{id}")
        end
        
        def get_ano(id)
            self.class.get("https://parallelum.com.br/fipe/api/v1/carros/marcas/23/modelos/#{id}")
        end

        def get_preco(id)
            self.class.get("https://parallelum.com.br/fipe/api/v1/carros/marcas/23/modelos/5637/anos/#{id}")
        end
    end
end